import React from 'react';
import {TouchableWithoutFeedback} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const IconAppComponent = ({name, size, color, onPres}) => {
    return (
        <TouchableWithoutFeedback onPress={onPres}>
            <Icon name={name} size={size} color={color}/>
        </TouchableWithoutFeedback>
    );
};

export default IconAppComponent;
