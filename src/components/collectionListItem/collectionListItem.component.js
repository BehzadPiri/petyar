import React from 'react';
import {CollectionItem} from '../index';
import {ScrollView,StyleSheet} from 'react-native';


const CollectionListItemComponent = ({onPres}) => {

    return (

        <ScrollView style={Styles.container}>
            <CollectionItem
                infoName="توله سگ شیتزو به همراه وسایل کامل"
                sector="سعادت آباد"
                img={require('../../assets/dag_item.png')}
                city="تهران"
                price="2000000"
                time="1ساعت پیش"
                onPres={onPres}
            />
            <CollectionItem
                infoName="گربه پرشین دال فیس شناسنامه دار"
                sector="حبیب الله"
                img={require('../../assets/cat_item.png')}
                city="تهران"
                price="10000000"
                onPres={onPres}
                time="1ساعت پیش"
            />
            <CollectionItem
                infoName="عروس هلندی ماده رام"
                sector="دولاب"
                img={require('../../assets/bird_item.png')}
                city="تهران"
                price="800000"
                time="1ساعت پیش"
                onPres={onPres}
            />
            <CollectionItem
                infoName="خرگوش لوپ هلندی"
                sector="صادقیه"
                img={require('../../assets/robbit_item.png')}
                city="تهران"
                price="1500000"
                time="1ساعت پیش"
                onPres={onPres}
            />
            <CollectionItem
                infoName="توله سگ شیتزو به همراه وسایل کامل"
                sector="سعادت آباد"
                img={require('../../assets/dag_item.png')}
                city="تهران"
                price="2000000"
                time="1ساعت پیش"
                onPres={onPres}
            />
            <CollectionItem
                infoName="گربه پرشین دال فیس شناسنامه دار"
                sector="حبیب الله"
                img={require('../../assets/cat_item.png')}
                city="تهران"
                price="10000000"
                onPres={onPres}
                time="1ساعت پیش"
            />
            <CollectionItem
                infoName="عروس هلندی ماده رام"
                sector="دولاب"
                img={require('../../assets/bird_item.png')}
                city="تهران"
                price="800000"
                time="1ساعت پیش"
                onPres={onPres}
            />
            <CollectionItem
                infoName="خرگوش لوپ هلندی"
                sector="صادقیه"
                img={require('../../assets/robbit_item.png')}
                city="تهران"
                price="1500000"
                time="1ساعت پیش"
                onPres={onPres}
            />
        </ScrollView>

    );
};

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        width: '100%',
        paddingBottom: 80,
    },
});

export default CollectionListItemComponent;
