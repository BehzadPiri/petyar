import {RFPercentage} from 'react-native-responsive-fontsize';
import {Dimensions, StyleSheet} from 'react-native';

const {width} = Dimensions.get('window');

const Styles = StyleSheet.create({
    RegisterInput: {
        width: width * 0.8,
        height: 40,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#00766A',
        fontSize: RFPercentage(2.5),
        fontWeight: '200',
        textAlign: 'center',
    },
    authInput: {
        width: width * 0.8,
        height: 40,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#00766A',
        fontSize: RFPercentage(2.7),
        fontWeight: '200',
        textAlign: 'center',
        marginBottom: 15,
        zIndex: 3,

    },
    userInput: {
        width: '100%',
        height: 45,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#aaaaaa',
        fontSize: RFPercentage(2.5),
        fontWeight: '200',
        textAlign: 'right',
        marginBottom: 15,
        paddingLeft: 15,
        marginTop: 10,
    },
    userInputEditable: {
        width: '100%',
        height: 45,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#aaaaaa',
        fontSize: RFPercentage(2.5),
        fontWeight: '200',
        textAlign: 'right',
        marginBottom: 15,
        paddingLeft: 15,
        marginTop: 10,
        backgroundColor: '#EBEBEB',
        zIndex: 3,
    },
    searchInput: {
        width: '100%',
        height: 45,
        borderRadius: 5,
        fontSize: RFPercentage(2.6),
        fontWeight: '200',
        textAlign: 'right',
        marginBottom: 15,
        paddingLeft: 15,
        marginTop: 10,
        backgroundColor: '#EAEAEA',
    },
});

export default Styles;
