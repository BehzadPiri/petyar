import React from 'react';
import {TextInput} from 'react-native';
import Styles from './inputApp.style';

const InputAppComponent = ({
                               max,
                               placeholder,
                               keyboardType,
                               phone,
                               numberOfLines,
                               auth,
                               user,
                               editable,
                               disable,
                               children,
                               search,
                           }) => {


    return (
        <TextInput
            editable={editable}
            keyboardType={keyboardType}
            placeholder={placeholder}
            numberOfLines={numberOfLines}
            maxLength={max}
            style={[
                phone === true ? Styles.RegisterInput : null,
                auth === true ? Styles.authInput : null,
                user === true ? Styles.userInput : null,
                disable === true ? Styles.userInputEditable : null,
                search === true ? Styles.searchInput : null,
            ]}
        >
            {children}
        </TextInput>
    );
};

export default InputAppComponent;
