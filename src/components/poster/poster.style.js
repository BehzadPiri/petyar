import {RFPercentage} from 'react-native-responsive-fontsize';
import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({

    containerPoster: {
        width: '100%',
        height: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        marginBottom: 10,
    },
    posterText: {
        color: 'black',
        fontSize: RFPercentage(3),
    },
    posterBtn: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    posterBtnTxt: {
        color: '#6AB1D6',
        marginRight: 5,
        fontSize: RFPercentage(2),
    },

});

export default Styles;
