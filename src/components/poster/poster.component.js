import {Text, TouchableWithoutFeedback, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import React from 'react';
import Styles from './poster.style';

const PosterComponent = () => {
    return (
        <View style={Styles.containerPoster}>

            <Text style={Styles.posterText}>
                آخرین آگهی ها
            </Text>

            <TouchableWithoutFeedback>
                <View style={Styles.posterBtn}>
                    <Text style={Styles.posterBtnTxt}>مشاهده آگهی ها</Text>
                    <Icon name="angle-left" color={'#6AB1D6'} size={14}/>
                </View>
            </TouchableWithoutFeedback>

        </View>
    );
};

export default PosterComponent;
