import React from 'react';
import {Appbar} from 'react-native-paper';
import Styles from './headerApp.style';

const HeaderApp = ({children}) => {


    return (
        <Appbar.Header style={Styles.Header}>
            {children}
        </Appbar.Header>
    );
};

export default HeaderApp;
