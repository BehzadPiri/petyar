import {Dimensions, StyleSheet} from 'react-native';

const {height, width} = Dimensions.get('window');

const Styles = StyleSheet.create({
    Header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 5,
        backgroundColor: 'transparent',
        paddingTop: 15,
        height: 70,
    },
});

export default Styles;
