import React from 'react';
import {Select} from '@mobile-reality/react-native-select-pro';
import {Dimensions} from 'react-native';

const {width} = Dimensions.get('window');

const SelectComponent = ({DATA, text, city}) => {
    return (
        <Select
            placeholderText={text}
            optionTextStyle={{fontSize: 16}}
            optionStyle={{backgroundColor: 'transparent', height: 40}}
            options={DATA}

            optionsListStyle={city === true ? {
                maxHeight: 150,
                borderColor: '#aaaaaa',
                marginLeft: width / -2.1,
            } : {maxHeight: 150, borderColor: '#aaaaaa'}}
            optionSelectedStyle={{backgroundColor: '#bebebe'}}
            selectControlTextStyle={{fontSize: 17, backgroundColor: 'transparent'}}
            selectContainerStyle={{width: '100%'}}
            selectControlStyle={{
                borderRadius: 25,
                borderColor: '#aaaaaa',
                borderWidth: 1,
                marginTop: 7,
                height: 45,
                paddingLeft: 10,
            }}

        />
    );

};

export default SelectComponent;
