import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import Styles from './btnApp.style';
import Icon from 'react-native-vector-icons/FontAwesome5';

const BtnApp = ({
                    text,
                    onPressBtn,
                    location,
                    user,
                    btnLeft,
                    btnLeftIcon,
                    btnRight,
                    btnRightIcon,
                    edit,
                    editIcon,
                    center,
                }) => {
    return (
        <TouchableOpacity
            onPress={onPressBtn}
            style={[
                center === true ? Styles.btnCenter : null,
                location === true ? Styles.btnLocation : null,
                user === true ? Styles.btnUser : null,
                btnLeft === true ? Styles.btnLeft : null,
                btnRight === true ? Styles.btnRight : null,
                edit === true ? Styles.btnEdit : null,
            ]}
        >

            {[
                btnLeftIcon ? (<Icon size={18} color={'#6AB1D6'} name={btnLeftIcon}/>) : null,
                btnRightIcon ? (<Icon size={18} color={'white'} name={btnRightIcon}/>) : null,
                    editIcon ? (<Icon size={18} color={'#ACACAC'} name={editIcon}/>) : null,
            ]}

            <Text
                style={[
                    center || user === true ? Styles.btnCenterText : null,
                    location === true ? Styles.btnLocationText : null,
                    btnRight === true ? Styles.btnRightText : null,
                    btnLeft === true ? Styles.btnLeftText : null,
                    edit === true ? Styles.editText : null,
                ]}>
                {text}
            </Text>


        </TouchableOpacity>
    );
};

export default BtnApp;
