import {Dimensions, StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width} = Dimensions.get('window');

const Styles = StyleSheet.create({
    btnCenter: {
        width: width * 0.8,
        height: 40,
        borderRadius: 30,
        fontWeight: '200',
        textAlign: 'center',
        marginBottom: 15,
        backgroundColor: '#6AB1D6',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnCenterText: {
        fontSize: RFPercentage(2.8),
        color: 'white',
        marginHorizontal: 8,
    },
    btnLocation: {
        width: '95%',
        height: 40,
        borderRadius: 10,
        fontWeight: '200',
        textAlign: 'center',
        marginBottom: 15,
        backgroundColor: '#EDF6FD',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnLocationText: {
        fontSize: RFPercentage(2.8),
        color: '#4DA9E4',
    },
    btnUser: {
        width: '100%',
        height: 40,
        borderRadius: 30,
        fontWeight: '200',
        textAlign: 'center',
        marginBottom: 15,
        backgroundColor: '#6AB1D6',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    btnLeft: {
        width: '48%',
        height: 45,
        borderRadius: 30,
        fontWeight: '200',
        textAlign: 'center',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#6AB1D6',
        borderWidth: 1,
        flexDirection: 'row',
    },
    btnRight: {
        width: '48%',
        height: 45,
        borderRadius: 30,
        fontWeight: '200',
        textAlign: 'center',
        backgroundColor: '#6AB1D6',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',

    },
    btnRightText: {
        color: '#ffffff',
        fontSize: RFPercentage(2.7),
        fontWeight: 'bold',
        marginLeft: 8,
    },
    btnLeftText: {
        color: '#6AB1D6',
        marginLeft: 8,
        fontSize: RFPercentage(2.7),
        fontWeight: 'bold',
    },
    btnRightIcon: {
        color: '#ffffff',
    },
    btnLeftIcon: {
        color: '#6AB1D6',
    },
    btnEdit: {
        width: '100%',
        height: 40,
        borderRadius: 10,
        fontWeight: '200',
        textAlign: 'center',
        marginBottom: 15,
        backgroundColor: '#FBFBFB',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection:"row"
    },
    editText: {
        fontSize: RFPercentage(2.6),
        color: '#ACACAC',
        marginLeft:4
    },
});

export default Styles;
