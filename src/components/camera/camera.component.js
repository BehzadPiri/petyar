import React, {Component} from 'react';
import {Dimensions, Image, PermissionsAndroid, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {launchCamera} from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './camera.style';

const {width} = Dimensions.get('window');

class CameraComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {images: []};
    }

    render() {

        const camera = async () => {

            const grantedcamera = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: 'App Camera Permission',
                    message: 'App needs access to your camera ',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            const grantedstorage = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    title: 'App Camera Permission',
                    message: 'App needs access to your camera ',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (grantedcamera === PermissionsAndroid.RESULTS.GRANTED && grantedstorage === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('Camera & storage permission given');

                let options = {
                    mediaType: 'photo',
                    saveToPhotos: true,
                    includeBase64: false,
                    maxWidth: width,
                    maxHeight: width,
                };

                await launchCamera(options, (res) => {

                    if (res.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (res.errorCode) {
                        console.log('ImagePicker Error: ', res.errorCode);
                    } else if (res.errorMessage) {
                        console.log('User tapped custom button: ', res.errorMessage);
                        alert(res.errorMessage);
                    } else {
                        this.setState({images: [...this.state.images, res.assets]}, () => console.log('set', this.state.images));

                    }
                });


            } else {
                console.log('Camera permission denied');
            }
        };


        const {images} = this.state;

        return (
            <View style={Styles.container}>

                <Text style={Styles.cameraText}>عکس آگهی</Text>

                <ScrollView horizontal showsHorizontalScrollIndicator={false} style={Styles.coScroll}>

                    <TouchableOpacity onPress={camera} style={Styles.btnCamera}>
                        <View style={Styles.coCamera}>
                            <Icon name="image" color="#929292" size={40}/>
                            <Text style={Styles.coCameraText}>افزودن تصویر</Text>
                        </View>
                    </TouchableOpacity>

                    {
                        images.map((image) => image.map((img, index) => (

                            <View key={index} style={Styles.bgImg}>

                                <View style={Styles.coImg}>
                                    <Image style={Styles.img} source={{uri: img.uri}}/>
                                </View>

                            </View>
                        )))
                    }

                </ScrollView>
            </View>
        );

    };
}

export default CameraComponent;
