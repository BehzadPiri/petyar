import {StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const Styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 200,
        alignItems: 'flex-start',
        marginBottom: 10,
        marginTop: 30,
    },
    cameraText: {
        fontSize: RFPercentage(2.7),
        color: '#494949',
        marginBottom: 15,
    },
    coScroll: {
        flexDirection: 'row',
        paddingVertical: 5,
        flex: 1,
    },
    btnCamera: {
        backgroundColor: '#F5F5F5',
        width: 150,
        height: 150,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    coCamera: {
        backgroundColor: 'transparent',
        width: 135,
        height: 135,
        borderRadius: 10,
        borderWidth: 2,
        borderStyle: 'dashed',
        borderColor: '#929292',
        alignItems: 'center',
        justifyContent: 'center',
    },
    coCameraText: {
        fontSize: RFPercentage(2.7),
        color: '#929292',
    },
    bgImg: {
        backgroundColor: '#F5F5F5',
        width: 150,
        height: 150,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
    },
    coImg: {
        backgroundColor: 'transparent',
        width: 135,
        height: 135,
        borderRadius: 10,
    },
    img: {
        width: null,
        height: null,
        flex: 1,
    },
});

export default Styles;
