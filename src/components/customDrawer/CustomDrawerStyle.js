import {StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';


const Styles = StyleSheet.create({
    containerHeaderDrawer: {
        height: 150,
        backgroundColor: 'white',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: 15,
    },
    containerImg: {
        width: 80,
        height: 80,
    },
    img: {
        width: null,
        height: null,
        flex: 1,
        borderRadius: 40,
    },
    containerText: {
        marginHorizontal: 20,
    },
    text: {
        fontSize: RFPercentage(2.4),
        fontWeight: 'bold',
        marginBottom: 5,
        color: '#000000',
    },
    drawerItem: {
        marginLeft: -20,
        fontSize: RFPercentage(2.4),
        color: '#000',
    },
});

export default Styles;
