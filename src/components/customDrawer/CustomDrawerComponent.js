import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Alert, BackHandler, Image, Text, TouchableOpacity, View} from 'react-native';
import {Divider} from 'react-native-paper';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import {About, Contact, User} from '../../screens';
import Styles from './CustomDrawerStyle';


const bullhornIcon = () => <Icon name="bullhorn" size={17} color={'#6AB1D6'}/>;
const commentsIcon = () => <Icon name="comments" size={20} color={'#6AB1D6'}/>;
const phoneIcon = () => <Icon name="phone" size={17} color={'#6AB1D6'}/>;
const exitIcon = () => <Icon name="arrow-circle-right" size={20} color={'#6AB1D6'}/>;
const infoIcon = () => <Icon name="info-circle" size={20} color={'#6AB1D6'}/>;


const CustomDrawerComponent = (props) => {
    return (
        <View style={{flex: 1}}>

            <View style={Styles.containerHeaderDrawer}>

                <View style={Styles.containerImg}>
                    <Image style={Styles.img} source={require('../../assets/bgLabel.png')}/>
                </View>

                <View style={Styles.containerText}>

                    <Text style={Styles.text}>نیما قربانی بیاتی</Text>

                    <TouchableOpacity onPress={() => props.navigation.navigate('User')}>
                        <Text>مشاهده اطلاعات کاربری</Text>
                    </TouchableOpacity>

                </View>

            </View>

            <Divider/>

            <DrawerContentScrollView {...props}>

                <DrawerItem label={'آگهی های من'} icon={bullhornIcon} labelStyle={Styles.drawerItem}
                            onPress={() => props.navigation.navigate('MyAds')}/>

                <DrawerItem label={'مشاوره ها'} icon={commentsIcon} labelStyle={Styles.drawerItem}/>

                <DrawerItem label={'درباره ما'} icon={infoIcon} labelStyle={Styles.drawerItem}
                            onPress={() => props.navigation.navigate('About')}/>

                <DrawerItem label={'تماس با ما'} icon={phoneIcon} labelStyle={Styles.drawerItem}
                            onPress={() => props.navigation.navigate('Contact')}/>

                <DrawerItem label={'خروج'} icon={exitIcon} labelStyle={Styles.drawerItem}
                            onPress={() => Alert.alert('خروج از برنامه',
                                'آیا برای خروج از برنامه مطمئن هستید؟',
                                [
                                    {
                                        text: 'بله',
                                        style: 'default',
                                        onPress: () => BackHandler.exitApp(),
                                    },
                                    {
                                        text: 'خیر',
                                        style: 'cancel',
                                    },
                                ], {cancelable: true},
                            )}/>
            </DrawerContentScrollView>

        </View>
    );
};

export default CustomDrawerComponent;

