import {Dimensions, StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width} = Dimensions.get('window');

const Styles = StyleSheet.create({
    container: {
        width: "100%",
        height: 120,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#ECECEC',
        paddingHorizontal: 5,
        marginBottom: 15,
    },
    containerImg: {
        width: width*0.26,
        height: width*0.26,
        overflow: 'hidden',
        borderRadius: 20,
    },
    Img: {
        width: null,
        height: null,
        flex: 1,
        borderRadius: 20,
    },
    containerInfo: {
        justifyContent: 'space-around',
        alignItems: 'flex-start',
        paddingLeft: 10,
        width: width*0.7,
        height: '100%',
    },
    infoName: {
        color: '#000',
        width: '100%',
        fontSize: RFPercentage(2.6),
        height: '45%',
        marginTop: 10,
        fontWeight:"bold"
    },
    infoCity:{
        fontSize: RFPercentage(2.3)
    }
});

export default Styles;
