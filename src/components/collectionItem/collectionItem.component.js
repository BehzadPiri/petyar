import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from './collectionItem.style';

const CollectionItemComponent = ({img, infoName, time, city, price, sector, onPres}) => {

    return (
        <TouchableOpacity style={Styles.container} onPress={onPres} activeOpacity={1}>

            <View style={Styles.containerImg}>
                <Image style={Styles.Img} source={img}/>
            </View>

            <View style={Styles.containerInfo}>

                <Text style={Styles.infoName}>{infoName}</Text>

                <View style={{flexDirection: 'row'}}>
                    <Text style={Styles.infoCity}>{time}</Text>
                    <Text style={Styles.infoCity}>{city}،</Text>
                    <Text style={Styles.infoCity}>{sector}</Text>
                </View>

                <Text style={[Styles.infoCity, {marginBottom: 15}]}>{price} تومان </Text>

            </View>

        </TouchableOpacity>

    );

};

export default CollectionItemComponent;
