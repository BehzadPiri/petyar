import {Dimensions, StyleSheet} from 'react-native';

const {height} = Dimensions.get('window');

const Styles = StyleSheet.create({
    container: {
        width: '100%',
        height: height * 0.44,
        backgroundColor: 'white',
        flexWrap: 'wrap',
        flexDirection: 'row',
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },

});


export default Styles;
