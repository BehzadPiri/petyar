import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import Styles from './caregoryListItem.style';
import {CategoryItem} from '../index';


const CategoryListItem = ({onprs}) => {

    return (

        <View style={Styles.container}>

            <TouchableOpacity onPress={onprs}>
                <CategoryItem name="گربه" img={require('../../assets/cat.png')} bgColor={'#EBD5C3'}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={onprs}>
                <CategoryItem name="سگ" img={require('../../assets/dog.png')} bgColor={'#F3CFCF'}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={onprs}>
                <CategoryItem name="پرنده" img={require('../../assets/brid.png')} bgColor={'#F5E1AF'}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={onprs}>
                <CategoryItem name="خرگوش" img={require('../../assets/rabbit.png')} bgColor={'#DEEBC3'}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={onprs}>
                <CategoryItem name="موش" img={require('../../assets/mouse.png')} bgColor={'#FFE9D1'}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={onprs}>
                <CategoryItem name="ماهی" img={require('../../assets/fish.png')} bgColor={'#D1EAFF'}/>
            </TouchableOpacity>

        </View>
    );
};

export default CategoryListItem;
