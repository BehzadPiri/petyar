import {Dimensions, StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width} = Dimensions.get('window');

const Styles = StyleSheet.create({

    containerItem: {
        width: width * 0.28,
        height: width * 0.28,
        padding: 10,
        borderTopRightRadius: 35,
        borderTopLeftRadius: 35,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom:20,
        margin: 7,
    },
    text: {
        fontSize: RFPercentage(2.5),
        color: '#222222',
        bottom: 20,
    },
    containerInfo: {
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    containerImg: {
        width: 95,
        height: 100,
        bottom: 30,
    },
    img: {
        width: null,
        height: null,
        flex: 1,
    },
});


export default Styles;
