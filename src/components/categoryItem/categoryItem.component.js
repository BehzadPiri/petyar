import React from 'react';
import {Image, Text, View} from 'react-native';
import Styles from './categoryItem.style';

const CategoryItem = ({bgColor, img, name}) => {

    return (

        <View style={[{backgroundColor: bgColor}, Styles.containerItem]}>

            <View style={Styles.containerInfo}>

                <View style={Styles.containerImg}>

                    <Image style={Styles.img} source={img}/>

                </View>

                <Text style={Styles.text}>{name}</Text>

            </View>

        </View>
    );
};

export default CategoryItem;

