import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Advertising, Conference, Register} from '../screens';
import DrawerNavigation from './DrawerNavigation';


const Stack = createNativeStackNavigator();

class StackNavigation extends Component {
    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator
                    initialRouteName={'DrawerNavigation'}
                    screenOptions={{headerShown: false, barStyle: {backgroundColor: 'white'}}}>
                    <Stack.Screen name={'Register'} component={Register}/>
                    <Stack.Screen name={'DrawerNavigation'} children={DrawerNavigation}/>
                    <Stack.Screen name={'Advertising'} children={Advertising}/>
                    <Stack.Screen name={'Conference'} children={Conference}/>
                </Stack.Navigator>
            </NavigationContainer>
        );
    }
}

export default StackNavigation;
