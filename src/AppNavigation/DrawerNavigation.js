import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {About, Ads, Advertising, Contact, Home, ImageAds, MyAds, NewAds, Search, User} from '../screens';
import {CustomDrawer} from '../components';

const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {

    return (

        <Drawer.Navigator initialRouteName={'NewAds'}
                          screenOptions={{headerShown: false}}
                          drawerContent={(props) => <CustomDrawer {...props} />}
        >
            <Drawer.Screen name="Home" component={Home}/>
            <Drawer.Screen name="Contact" component={Contact}/>
            <Drawer.Screen name="About" component={About}/>
            <Drawer.Screen name="User" component={User}/>
            <Drawer.Screen name="Ads" component={Ads}/>
            <Drawer.Screen name="Search" component={Search}/>
            <Drawer.Screen name="Advertising" component={Advertising}/>
            <Drawer.Screen name="MyAds" component={MyAds}/>
            <Drawer.Screen name="NewAds" component={NewAds}/>
            <Drawer.Screen name="ImageAds" component={ImageAds}/>

        </Drawer.Navigator>
    );

};

export default DrawerNavigation;
