import React, {Component} from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Appbar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './myAdsStyle';
import {BtnApp} from '../../components';

class MyAdsScreen extends Component {
    render() {
        return (
            <View style={Styles.container}>

                <Appbar.Header style={Styles.header}>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>

                        <Icon name={'arrow-right'} color={'#4E4E4E'} size={24}/>

                    </TouchableOpacity>

                    <View style={Styles.coText}>

                        <Text style={Styles.headerText}>آگهی های من</Text>

                    </View>

                </Appbar.Header>

                <ScrollView style={Styles.coAds}>

                    <View style={Styles.ads}>

                        <View style={Styles.adsHeader}>

                            <View style={Styles.coImg}>

                                <Image style={Styles.img} source={require('../../assets/dag_item.png')}/>

                            </View>

                            <View style={{margin: 10}}>

                                <Text style={Styles.title}>توله سگ شیتزو به همراه وسایل کامل</Text>

                                <View style={Styles.coAdsText}>
                                    <Text style={Styles.text}>1 ساعت پیش</Text>
                                    <Text style={Styles.text}>در تهران،</Text>
                                    <Text style={Styles.text}>سعادت آباد</Text>
                                </View>

                                <Text style={Styles.text}>2،000،000 تومان</Text>

                            </View>

                        </View>

                        <View style={{width: '100%', padding: 5, flexDirection: 'row'}}>
                            <View style={{width: '65%', height: '100%'}}>
                                <BtnApp location={true} text="مشاهده جزییات"/>
                            </View>
                            <View style={{width: '35%', height: '100%'}}>
                                <BtnApp edit={true} text="ویرایش آگهی" editIcon={'edit'}/>
                            </View>
                        </View>

                    </View>


                    <View style={Styles.ads}>

                        <View style={Styles.adsHeader}>

                            <View style={Styles.coImg}>

                                <Image style={Styles.img} source={require('../../assets/dag_item.png')}/>

                            </View>

                            <View style={{margin: 10}}>

                                <Text style={Styles.title}>توله سگ شیتزو به همراه وسایل کامل</Text>

                                <View style={Styles.coAdsText}>
                                    <Text style={Styles.text}>1 ساعت پیش</Text>
                                    <Text style={Styles.text}>در تهران،</Text>
                                    <Text style={Styles.text}>سعادت آباد</Text>
                                </View>

                                <Text style={Styles.text}>2،000،000 تومان</Text>

                            </View>

                        </View>

                        <View style={{width: '100%', padding: 5, flexDirection: 'row'}}>
                            <View style={{width: '65%', height: '100%'}}>
                                <BtnApp location={true} text="مشاهده جزییات"/>
                            </View>
                            <View style={{width: '35%', height: '100%'}}>
                                <BtnApp edit={true} text="ویرایش آگهی" editIcon={'edit'}/>
                            </View>
                        </View>

                    </View>

                </ScrollView>

                <View style={{top: 10}}>
                    <BtnApp text="ثبت آگهی جدید" user={true} btnRightIcon="plus"
                            onPressBtn={() => this.props.navigation.navigate('NewAds')}/>
                </View>

            </View>
        );
    }
}

export default MyAdsScreen;
