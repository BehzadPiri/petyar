import {StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';


const Styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: 'white',
    },
    header: {
        backgroundColor: 'transparent',
        alignItems: 'center',
    },
    coText: {
        marginHorizontal: 130,
    },
    headerText: {
        fontSize: RFPercentage(2.9),
        color: 'black',
    },

    coAds: {
        width: '100%',
        backgroundColor: 'white',
        flex: 1,
        height: '100%',
    },
    ads: {
        width: '100%',
        height: 180,
        borderWidth: 1,
        borderColor: '#ECECEC',
        borderRadius: 20,
        marginBottom:10
    },
    adsHeader: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#ECECEC',
        padding: 5,
    },
    coImg: {
        width: 115,
        height: 115,
        borderRadius: 10,
        overflow: 'hidden',
    },
    img: {
        width: null,
        height: null,
        flex: 1,
    },
    title: {
        fontSize: RFPercentage(2.6),
        fontWeight: 'bold',
        color: 'black',
    },
    text: {
        fontSize: RFPercentage(2.4),
        color: 'black',
    },
    coAdsText: {
        flexDirection: 'row',
        marginTop: 20,
    },
});

export default Styles;
