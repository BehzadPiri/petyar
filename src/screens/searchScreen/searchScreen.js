import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {InputApp} from '../../components';
import Styles from './searchStyle';

const SearchScreen = (props) => {

    return (

        <View style={Styles.container}>

            <View style={Styles.searchBar}>

                <View style={{width: '88%'}}>

                    <InputApp search={true} placeholder={'جستجو...'}/>

                </View>

                <TouchableOpacity onPress={() => props.navigation.navigate('Home')} style={Styles.closeBtn}>

                    <Text style={{fontSize: 16, color: 'black'}}>بستن</Text>

                </TouchableOpacity>

            </View>

            <View style={Styles.emptyContainer}>

                <Image source={require('../../assets/search_img.png')}/>

                <Text style={Styles.textEmpty}>صفحه ی سرچ شما خالی می باشد</Text>

            </View>

        </View>
    );
};

export default SearchScreen;
