import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from './authenticationStyle';
import {BtnApp, InputApp} from '../../components';


class AuthenticationScreen extends Component {


    render() {

        const onPressBtn = () => {
            this.props.navigation.navigate('DrawerNavigation');
        };

        return (
            <View style={Styles.container}>


                <View style={Styles.containerViewTop}>

                    <Text style={Styles.viewTopTextOne}>کد احراز هویت</Text>

                    <Text style={Styles.viewTopTextTow}>کد مورد نظر به شماره تماس زیر ارسال شد</Text>

                    <View style={Styles.viewContainerTextNum}>

                        <Text style={Styles.textNum}>+981921234567</Text>

                        <TouchableOpacity style={Styles.touchBtn} onPress={() => this.props.navigation.goBack()}>

                            <Text style={Styles.btnText}>(تغییر شماره تماس)</Text>

                        </TouchableOpacity>

                    </View>

                </View>


                <View style={Styles.containerCenter}>

                    <Text style={Styles.authText}>کد احراز هویت</Text>

                    <InputApp
                        max={5}
                        keyboardType="phone-pad"
                        phone={true}
                        placeholder="- - - - -"
                        numberOfLines={5}
                        auth={true}/>

                    <View style={Styles.containerTime}>

                        <Text style={Styles.textTime}>زمان انقضای کد</Text>

                        <Text style={Styles.time}>2:48</Text>

                    </View>

                    <BtnApp center={true} text={'تایید کد'} onPressBtn={onPressBtn}/>

                </View>


                <View style={Styles.containerButton}>

                    <Image style={Styles.imgBtn} source={require('../../assets/footerBG.png')}/>

                </View>

            </View>
        );
    }
}

export default AuthenticationScreen;
