import {Dimensions, StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width, height} = Dimensions.get('window');

const Styles = StyleSheet.create({

    /////////////////////////////top

    container: {
        flex: 1,
        backgroundColor: 'white',
    },

    containerViewTop: {
        alignItems: 'center',
        justifyContent: 'space-around',
        padding: 20,
        width: width,
        height: height * 0.3,
    },

    viewTopTextOne: {
        fontSize: RFPercentage(5),
        color: 'black',
        fontWeight: 'bold',
    },
    viewTopTextTow: {
        fontSize: RFPercentage(2.7),
        fontWeight: '300',
    },
    viewContainerTextNum: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: width * 0.7,
    },
    textNum: {
        fontSize: RFPercentage(3),
        fontWeight: 'normal',
    },
    touchBtn: {
        backgroundColor: 'transparent',
    },
    btnText: {
        color: '#6AB1D6',
        fontSize: RFPercentage(2.2),
    },

    ///////////////////////////////////////center

    containerCenter: {
        alignItems: 'center',
        width: width,
        height: height * 0.5,
        paddingTop: 20,
        paddingBottom: 40,
    },
    authText: {
        fontSize: RFPercentage(2.5),
        marginBottom: 15,
    },

    containerTime: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
    },
    textTime: {
        fontSize: RFPercentage(2.8),
        marginRight: 5,
    },
    time: {
        fontSize: RFPercentage(2.2),
    },

    ////////////////////////////////////////////button

    containerButton: {
        width: width,
        height: height * 0.2,
    },
    imgBtn: {
        width: null,
        height: null,
        flex: 0.5,
    },
});

export default Styles;
