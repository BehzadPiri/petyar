import {StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
    },
    header: {
        backgroundColor: 'transparent',
    },
    textAds: {
        fontSize: RFPercentage(3),
        color: 'black',
        fontWeight: '400',
    },
    coGrouping: {
        alignItems: 'flex-start',
        width: '100%',
        justifyContent: 'center',
    },
    textGrouping: {
        fontSize: RFPercentage(2.4),
        marginLeft: 20,
        color: 'black',
    },
    coPrice: {
        width: '100%',
        height: 45,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#aaaaaa',
        marginBottom: 15,
        overflow: 'hidden',
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    price: {
        borderWidth: null,
        borderColor: null,
        width: '80%',
        height: '100%',
        paddingLeft: 15,
        fontSize: RFPercentage(2.7),
    },
    btnPrice: {
        width: '20%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F9F9F9',
    },
    textPrice: {
        fontSize: RFPercentage(2.4),
        color: '#BEBEBE',
    },
    location: {
        flexDirection: 'row',
    },
    locationCity: {
        alignItems: 'flex-start',
        width: '50%',
        justifyContent: 'center',
        marginBottom: 25,
    },
    place: {
        width: '48%',
        marginLeft: 5,
        marginBottom: 15,
    },
    coDescription: {
        width: '100%',
        bottom: 15,
    },
    description:{
        width: '100%',
        height: 130,
        borderWidth: 1,
        borderColor: '#bebebe',
        borderRadius: 20,
        alignItems: 'flex-start',
        paddingLeft: 20,
        justifyContent: 'flex-end', marginTop: 10,
        fontSize: RFPercentage(2.6),
        color: 'black',
        textAlignVertical: 'top',
    }
});

export default Styles;
