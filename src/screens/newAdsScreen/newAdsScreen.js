import React from 'react';
import {BtnApp, InputApp, Select} from '../../components';
import {SelectProvider} from '@mobile-reality/react-native-select-pro';
import {ScrollView, Text, TextInput, TouchableOpacity, View} from 'react-native';
import Styles from './newAdsStyle';
import {Appbar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5';

const DATA = [
    {value: 1, label: 'گربه'},
    {value: 2, label: 'سگ'},
    {value: 3, label: 'پرنده'},
    {value: 4, label: 'خرگوش '},
    {value: 5, label: 'موش '},
    {value: 6, label: 'ماهی'},
];
const DATA_CITY = [
    {value: 1, label: 'تهران'},
    {value: 2, label: 'اصفهان'},
    {value: 3, label: 'شیراز'},
    {value: 4, label: 'تبریز '},
    {value: 5, label: 'مشهد '},
    {value: 6, label: 'مازندران'},
];

class NewAdsScreen extends React.Component {
    render() {

        return (
            <SelectProvider>
                <ScrollView style={Styles.container}>

                    <Appbar.Header style={Styles.header}>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('MyAds')}>

                            <Icon name={'arrow-right'} color={'#4E4E4E'} size={24}/>

                        </TouchableOpacity>

                        <View style={{marginLeft: 18}}>

                            <Text style={Styles.user}>ثبت آگهی جدید</Text>

                        </View>

                    </Appbar.Header>

                    <View style={Styles.coGrouping}>
                        <Text style={Styles.textGrouping}>دسته بندی</Text>
                        <Select DATA={DATA} text="دسته بندی مورد نظر خود را انتخاب کنید"/>
                    </View>

                    <View style={{marginTop: 10}}>
                        <Text style={Styles.textGrouping}>عنوان آگهی</Text>
                        <InputApp user={true} placeholder="عنوان آگهی" keyboardType={'default'} max={1000}/>
                    </View>

                    <View>

                        <Text style={Styles.textGrouping}>قیمت</Text>

                        <View style={Styles.coPrice}>

                            <TextInput keyboardType="number-pad" placeholder="قیمت" textAlign="right"
                                       style={Styles.price}/>

                            <TouchableOpacity activeOpacity={1} style={Styles.btnPrice}>

                                <Text style={Styles.textPrice}>تومان</Text>

                            </TouchableOpacity>

                        </View>

                    </View>

                    <View style={Styles.location}>

                        <View style={Styles.locationCity}>
                            <Text style={Styles.textGrouping}>شهر</Text>
                            <Select DATA={DATA_CITY} text="شهر" city={true}/>
                        </View>

                        <View style={Styles.place}>
                            <Text style={Styles.textGrouping}>محل</Text>
                            <InputApp user={true} keyboardType={'default'} placeholder={'محل'}/>
                        </View>

                    </View>

                    <View style={Styles.coDescription}>

                        <Text style={Styles.textGrouping}>توضیحات آگهی</Text>

                        <TextInput
                            placeholder="توضیحات آگهی"
                            textAlign="right"
                            numberOfLines={4}
                            multiline
                            style={Styles.description}
                        />

                    </View>


                    <BtnApp
                        user={true} text={'تایید و ادامه'}
                        onPressBtn={() => this.props.navigation.navigate('ImageAds')}
                    />

                </ScrollView>

            </SelectProvider>
        );
    }
}


export default NewAdsScreen;
