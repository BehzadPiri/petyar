import {Dimensions, StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width} = Dimensions.get('window');

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
    },
    containerImgHeader: {
        width: 60,
        height: 25,
        marginLeft: width * 0.35,
    },
    imgHeader: {
        width: null,
        height: null,
        flex: 1,
    },
    bodyContainer: {
        alignItems: 'center',
        marginTop: 20,
    },
    bodyContainerBg: {
        width: 210,
        height: 210,
        backgroundColor: '#F0F7FB',
        borderRadius: 105,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textContainerBody: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
    },
    textBody: {
        color: '#488EB3',
        fontSize: RFPercentage(3),
    },
    aboutContainer: {
        marginTop: 20,
        marginBottom: 20,
    },
    aboutText: {
        fontSize: RFPercentage(2.5),
        lineHeight: 30,
    },
});

export default Styles;
