import React, {Component} from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Appbar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './aboutStyle';


class AboutScreen extends Component {
    render() {
        return (
            <View style={Styles.container}>

                <Appbar.Header style={{backgroundColor: 'transparent'}}>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                        <Icon name={'arrow-right'} color={'#4E4E4E'} size={24}/>
                    </TouchableOpacity>

                    <View style={Styles.containerImgHeader}>
                        <Image style={Styles.imgHeader} source={require('../../assets/petyar.png')}/>
                    </View>

                </Appbar.Header>

                <View style={Styles.bodyContainer}>

                    <View style={Styles.bodyContainerBg}>
                        <Image source={require('../../assets/petyar_logo.png')}/>
                    </View>

                    <View style={Styles.textContainerBody}>
                        <Text style={Styles.textBody}>همه چیز در مورد پت یار</Text>
                    </View>

                </View>

                <ScrollView style={Styles.aboutContainer}>
                    <Text letterSpacing={6} style={Styles.aboutText}>
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است،
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است،
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است،
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است،
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است،
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است،
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است،
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است،
                    </Text>
                </ScrollView>

            </View>
        );
    }
}

export default AboutScreen;
