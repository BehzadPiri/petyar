import {StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const Styles = StyleSheet.create({

    containerBanner: {
        backgroundColor: 'lightsteelblue',
        width: '95%',
        height: '10%',
        marginLeft: 10, marginBottom: 20,
        borderRadius: 20,
        overflow: 'hidden',
    },
    imgBanner: {
        width: null,
        height: null,
        flex: 1,
    },
    containerBannerText: {
        flex: 1,
        width: '50%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bannerText: {
        color: '#6D520A',
        fontSize: RFPercentage(3.5),
        fontWeight: 'bold',
    },
    textBest: {
        color: '#6D520A',
        fontSize: RFPercentage(2.65),
    },
    Btn: {
        width: 120,
        height: 25,
        backgroundColor: '#6AB1D6',
        flexDirection: 'row',
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 8,
    },
    containerBtn: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
        marginTop: 10,
    },

});

export default Styles;
