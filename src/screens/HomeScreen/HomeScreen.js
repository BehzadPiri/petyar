import React, {Component} from 'react';
import {Image, ImageBackground, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Styles from './HomeStyle';
import {BtnApp, CategoryListItem, CollectionListItem, HeaderApp, IconApp, Poster} from '../../components';

class HomeScreen extends Component {

    render() {

        return (
            <View style={{backgroundColor: 'white', flex: 1, padding: 5}}>
                <ScrollView>

                    <HeaderApp>
                        <IconApp name="align-justify" size={25} color={'#000'}
                                 onPres={() => this.props.navigation.toggleDrawer()}/>

                        <View style={{width: 70, height: 30}}>
                            <Image style={{width: null, height: null, flex: 1}}
                                   source={require('../../assets/petyar.png')}/>
                        </View>

                        <IconApp size={25} color={'#000'} name={'search'}
                                 onPres={() => this.props.navigation.navigate('Search')}/>
                    </HeaderApp>

                    <View style={Styles.containerBanner}>

                        <ImageBackground style={Styles.imgBanner} source={require('../../assets/homeBanner.png')}>

                            <View style={Styles.containerBannerText}>

                                <Text style={Styles.bannerText}>مشاوره و راهنمایی</Text>

                                <Text style={Styles.textBest}>برای انتخاب بهترین گزینه</Text>

                                <TouchableOpacity style={Styles.Btn}>

                                    <Text style={{color: 'white'}}>چت با کارشناسان</Text>

                                    <IconApp name="comments" size={14} color={'white'}/>

                                </TouchableOpacity>

                            </View>

                        </ImageBackground>

                    </View>

                    <CategoryListItem onprs={() => this.props.navigation.navigate('Ads')}/>

                    <Poster/>

                    <CollectionListItem onPres={() => this.props.navigation.navigate('Advertising')}/>

                    <View style={Styles.containerBtn}>

                        <BtnApp text="درخواست مشاوره" btnRight={true} key={1}
                                onPressBtn={() => this.props.navigation.navigate('Conference')}/>

                        <BtnApp text="ثبت آگهی" btnLeft={true} btnLeftIcon={'plus'} key={2}/>

                    </View>

                </ScrollView>


            </View>

        );
    }
}

export default HomeScreen;
