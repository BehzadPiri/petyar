import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {Appbar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {BtnApp, InputApp} from '../../components';
import Styles from './userStyle';

class UserScreen extends Component {

    render() {
        return (

            <View style={Styles.container}>

                <Appbar.Header style={Styles.header}>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>

                        <Icon name={'arrow-right'} color={'#4E4E4E'} size={24}/>

                    </TouchableOpacity>

                    <View style={{marginLeft: 18}}>

                        <Text style={Styles.user}>اطلاعات کاربر</Text>

                    </View>

                </Appbar.Header>

                <View style={Styles.infoUser}>

                    <View style={Styles.containerName}>

                        <Text style={Styles.label}>نام</Text>

                        <InputApp user={true} placeholder={'نام'} keyboardType={'default'} max={20}/>

                    </View>

                    <View>
                        <Text style={Styles.label}>نام خانوادگی</Text>
                        <InputApp user={true} placeholder={'نام خانوادگی'} keyboardType={'default'} max={20}/>
                    </View>

                    <View>
                        <Text style={Styles.label}>شماره تماس</Text>
                        <InputApp style={Styles.input} disable={true} placeholder={'09121234567'}
                                  keyboardType={'default'} max={20} editable={false}/>
                    </View>

                </View>

                <View style={Styles.btn}>
                    <BtnApp user={true} text={'تایید ویرایش'}/>
                </View>

            </View>
        );
    }
}

export default UserScreen;
