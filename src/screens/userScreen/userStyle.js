import {StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const Styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
        justifyContent: 'space-between',
    },
    header: {
        backgroundColor: 'transparent',
        alignItems: 'center',
    },
    user: {
        color: '#000000',
        fontSize: RFPercentage(3),
    },
    infoUser: {
        bottom: 100,
        marginTop: 20,
    },
    containerName: {
        marginTop: 20,
        paddingLeft: 7,
        justifyContent: 'flex-end',
    },
    label: {
        fontSize: RFPercentage(2.7),
        marginLeft: 15,
    },
    input: {
        backgroundColor: '#aaa',
    },
    btn: {
        bottom: 0,
        position: 'relative',
        width: '100%',
    },

});

export default Styles;
