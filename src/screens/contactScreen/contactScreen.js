import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {Appbar, Divider} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {BtnApp} from '../../components';
import Styles from './contactStyle';

class ContactScreen extends Component {
    render() {

        return (
            <View style={Styles.container}>

                <Appbar.Header style={Styles.header}>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>

                        <Icon name={'arrow-right'} color={'#4E4E4E'} size={24}/>

                    </TouchableOpacity>

                </Appbar.Header>


                <View style={Styles.headerImg}>

                    <View style={Styles.containerImg}>

                        <Image source={require('../../assets/call.png')}/>

                    </View>

                    <View style={Styles.containerText}>

                        <Text style={Styles.Phone}>اطلاعات تماس</Text>

                        <Text style={Styles.connect}>از طریق راه های ارتباطی زیر میتوانید با ما تماس بگیرید</Text>

                    </View>

                </View>

                <View style={Styles.info}>


                    <View style={Styles.infoPhoneContainer}>

                        <View style={Styles.infoPhone}>

                            <View style={Styles.phoneIcon}>

                                <Icon size={18} name={'phone-alt'} color={'white'}/>

                            </View>

                            <Text style={Styles.phone}>تلفن ثابت</Text>

                        </View>

                        <Text style={Styles.textPhone}>44018936</Text>

                    </View>


                    <View style={Styles.infoEmailContainer}>

                        <View style={Styles.infoEmail}>

                            <View style={Styles.emailIcon}>

                                <Icon size={20} name={'envelope'} color={'white'}/>

                            </View>

                            <Text style={Styles.email}>ایمیل</Text>

                        </View>

                        <View style={Styles.emailTextContainer}>

                            <Text style={Styles.emailText}>Petyar@gmail.com</Text>

                            <Text style={Styles.emailText}>lnfo@petyar.com</Text>

                        </View>

                    </View>


                    <View style={Styles.infoAddressContainer}>

                        <View style={Styles.infoAddress}>

                            <View style={Styles.addressIcon}>

                                <Icon size={20} name={'map-marker-alt'} color={'white'}/>

                            </View>

                            <Text style={Styles.address}>آدرس</Text>

                        </View>

                        <View style={Styles.containerAddress}>

                            <Text style={Styles.textAddress}>تهران، سعادت آباد، بلوار دریا، پلاک55، واحد 6</Text>

                        </View>

                    </View>

                    <View style={{marginTop: 20}}>
                        <BtnApp text={'مشاهده موقعیت مکانی'} location={true}/>
                    </View>


                </View>

                <Divider style={Styles.divider}/>

                <View style={{marginTop: 20}}>

                    <View>
                        <Text style={Styles.textPhone}>شبکه های اجتماعی ما را دنبال کنید</Text>
                    </View>

                    <View style={Styles.containerSocial}>

                        <TouchableOpacity activeOpacity={1}>
                            <Image source={require('../../assets/instagram.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={1}>
                            <Image source={require('../../assets/telgram.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={1}>
                            <Image source={require('../../assets/aparat.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={1}>
                            <Image source={require('../../assets/in.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={1}>
                            <Image source={require('../../assets/whatsapp.png')}/>
                        </TouchableOpacity>

                    </View>

                </View>

            </View>
        );
    }
}

export default ContactScreen;
