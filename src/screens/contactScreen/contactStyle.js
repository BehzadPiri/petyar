import {Dimensions, StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width, height} = Dimensions.get('window');

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
    },
    header: {
        backgroundColor: 'transparent',
    },
    headerImg: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    containerImg: {
        backgroundColor: '#F0F7FB',
        width: 50,
        height: 50,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerText: {
        width: '85%',
        marginLeft: 10,
        justifyContent: 'space-between',
        height: 80,
    },
    Phone: {
        color: '#8F8F8F', fontSize: RFPercentage(3.5),
    },
    connect: {
        color: '#8F8F8F', fontSize: RFPercentage(2.7),
    },
    info: {
        alignItems: 'center',
    },
    infoPhoneContainer: {
        width: '100%',
        height: 45,
        backgroundColor: 'transparent',
        marginTop: 20,
        borderRadius: 100,
        borderWidth: 1.5,
        borderColor: '#DEDEDE',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 6,
    },
    infoPhone: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    phoneIcon: {
        width: 35,
        height: 35,
        borderRadius: 20,
        backgroundColor: '#4DA9E4',
        alignItems: 'center',
        justifyContent: 'center',
    },
    phone: {
        fontSize: RFPercentage(2.4),
        marginLeft: 12,
    },
    textPhone: {
        fontSize: RFPercentage(2.3),
    },
    infoEmailContainer: {
        width: '100%',
        height: 80,
        backgroundColor: 'transparent',
        marginTop: 15,
        borderRadius: 20,
        borderWidth: 1.5,
        borderColor: '#DEDEDE',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
    },
    infoEmail: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        bottom: 15,
    },
    emailIcon: {
        width: 35,
        height: 35,
        borderRadius: 20,
        backgroundColor: '#4DA9E4',
        alignItems: 'center',
        justifyContent: 'center',
    },
    email: {
        fontSize: RFPercentage(2.4),
        marginLeft: 12,
    },
    emailTextContainer: {
        alignItems: 'flex-end',
        justifyContent: 'space-around',
        height: '100%',
    },
    emailText: {
        fontSize: RFPercentage(2.2),
    },
    infoAddressContainer: {
        width: '100%',
        height: 80,
        backgroundColor: 'transparent',
        marginTop: 15,
        borderRadius: 20,
        borderWidth: 1.5,
        borderColor: '#DEDEDE',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },
    infoAddress: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: '50%',
    },
    addressIcon: {
        width: 35,
        height: 35,
        borderRadius: 20,
        backgroundColor: '#4DA9E4',
        alignItems: 'center',
        justifyContent: 'center',
    },
    address: {
        fontSize: RFPercentage(2.4),
        marginLeft: 12,
    },
    containerAddress: {
        height: '50%',
        top: 10,
    },
    textAddress: {
        fontSize: RFPercentage(2.5),
    },
    divider: {
        marginTop: 60,
        color: '#eeeeee',
        height: 1.5,
    },
    containerSocial: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 25,
    },
});

export default Styles;
