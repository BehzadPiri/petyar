import React from 'react';
import {ImageBackground, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Swiper from 'react-native-swiper';
import {BtnApp} from '../../components';
import {Appbar, Title} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './advertisingStyle';

const AdvertisingScreen = (props) => {
    return (
        <View style={Styles.container}>

            <View style={Styles.containerBg}>

                <Appbar.Header style={Styles.header}>

                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Icon name={'arrow-right'} color={'white'} size={24}/>
                    </TouchableOpacity>

                </Appbar.Header>

                <Swiper
                    autoplay={true}
                    dot={<View style={Styles.SwiperDot}/>}
                    activeDot={<View style={Styles.SwiperActiveDot}/>}
                >

                    <ImageBackground style={Styles.SwiperImg} source={require('../../assets/dag_item.png')}/>

                    <ImageBackground style={Styles.SwiperImg} source={require('../../assets/dag_item.png')}/>

                    <ImageBackground style={Styles.SwiperImg} source={require('../../assets/dag_item.png')}/>

                    <ImageBackground style={Styles.SwiperImg} source={require('../../assets/dag_item.png')}/>

                    <ImageBackground style={Styles.SwiperImg} source={require('../../assets/dag_item.png')}/>

                </Swiper>

            </View>

            <View style={Styles.containerInfo}>

                <View>
                    <Title style={Styles.title}>توله سگ شیتزو به همراه وسایل کامل</Title>
                    <View style={Styles.containerSubtitle}>
                        <Text style={Styles.subtitle}>1ساعت پیش</Text>
                        <Text style={Styles.subtitle}>در تهران،</Text>
                        <Text style={Styles.subtitle}>سعادت آباد</Text>
                    </View>
                </View>

                <View style={Styles.divider}/>

                <View style={Styles.containerSale}>

                    <View style={Styles.salePrice}>
                        <Text style={Styles.saleSize}>قیمت</Text>
                        <Text style={Styles.saleSize}>2،000،000 تومان</Text>
                    </View>

                    <View style={Styles.saleCategory}>
                        <Text style={Styles.saleSize}>دسته بندی</Text>
                        <Text style={[{color: '#6AB1D6'}, Styles.saleSize]}>سگ</Text>
                    </View>

                </View>

                <View style={Styles.divider}/>

                <View style={Styles.description}>

                    <Text style={Styles.saleSize}>توضیحات</Text>

                    <ScrollView>

                        <Text style={Styles.descriptionText}>
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک
                            است،
                            چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است،
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک
                            است،
                        </Text>

                    </ScrollView>

                </View>

                <View style={Styles.containerBtn}>

                    <BtnApp btnRight={true} text={'تملس تلفنی'} btnRightIcon={'phone'}/>

                    <BtnApp btnLeft={true} btnLeftIcon={'plus'} text={'ارسال پیامک'}/>

                </View>

            </View>

        </View>
    );
};

export default AdvertisingScreen;
