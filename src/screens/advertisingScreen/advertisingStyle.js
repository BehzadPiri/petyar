import {StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';


const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    containerBg: {
        width: '100%',
        height: '45%',
        shadowColor: '#eee',
        shadowOffset: {
            width: 4,
            height: 9,
        },
        shadowOpacity: 0,
        elevation: 12,
        borderWidth: 0.2,
        overflow: 'visible',
    },
    header: {
        backgroundColor: 'transparent',
        paddingLeft: 20,
        marginTop: 20,
        position: 'absolute',
        elevation: 2,
        zIndex: 2,
    },
    SwiperDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: 'rgba(255,255,255,0.4)',
        bottom: 40,
        margin: 4,
    },
    SwiperActiveDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: 'rgba(255,255,255,0.8)',
        bottom: 40,
        margin: 4,
    },
    SwiperImg: {
        flex: 1,
        height: null,
        width: null,
    },
    containerInfo: {
        backgroundColor: 'white',
        width: '95%',
        height: '70%',
        borderRadius: 20,
        padding: 10,
        elevation: 12,
        bottom: 35,
    },
    title: {
        fontsize: RFPercentage(2.8),
    },
    containerSubtitle: {
        flexDirection: 'row',
        marginBottom: 15,
    },
    subtitle: {
        fontSize: RFPercentage(2.5),
    },
    divider: {
        width: '100%',
        height: 1,
        backgroundColor: '#eee',
    },
    containerSale: {
        marginBottom: 15,
        marginTop: 15,
    },
    salePrice: {
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginBottom: 10,
    },
    saleSize: {
        fontSize: RFPercentage(2.9),
    },
    saleCategory: {
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    description: {
        marginTop: 10,
        height: 140,
    },
    descriptionText: {
        fontSize: RFPercentage(2.3),
        textAlign: 'justify',
    },
    containerBtn:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 15,
    }
});

export default Styles;
