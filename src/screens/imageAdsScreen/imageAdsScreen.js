import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Styles from './imageAdsStyle';
import {Appbar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {BtnApp, Camera} from '../../components';

class ImageAdsScreen extends Component {
    render() {
        return (
            <View style={Styles.container}>

                <Appbar.Header style={Styles.header}>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('NewAds')}>

                        <Icon name={'arrow-right'} color={'#4E4E4E'} size={24}/>

                    </TouchableOpacity>

                    <View style={{marginLeft: 15}}>

                        <Text style={Styles.textImg}>عکس آگهی</Text>

                    </View>

                </Appbar.Header>

                <View style={Styles.help}>

                    <Text style={Styles.textImg}>راهنمای افزودن عکس</Text>

                    <Text style={Styles.description}>
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک
                        است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است،لورم ایپسوم متن
                        ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است
                    </Text>

                </View>

                <Camera/>

                <View style={Styles.coBtn}>
                    <BtnApp text="ثبت آگهی" user={true}/>
                </View>

            </View>
        );
    }
}

export default ImageAdsScreen;
