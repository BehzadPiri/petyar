import {StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
    },
    header: {
        backgroundColor: 'transparent',
    },
    textImg: {
        fontSize: RFPercentage(2.8),
        color: '#000',
    },

    coBtn: {
        position: 'absolute',
        width: '100%',
        flex: 1,
        alignSelf: 'center',
        bottom: 0,
    },
    description: {
        textAlign: 'justify',
        fontSize: RFPercentage(2.8),
        color: '#5D5D5D',
        marginTop:10,
        lineHeight:30
    },
    help:{
        width: '100%',
        marginTop: 20
    }
});

export default Styles;
