import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Appbar, Title} from 'react-native-paper';
import {CollectionListItem, InputApp} from '../../components';
import Styles from './adsStyle';


const AdsScreen = (props) => {
    return (

        <View style={Styles.container}>

            <Appbar.Header style={Styles.header}>

                <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                    <Icon name={'arrow-right'} color={'#4E4E4E'} size={24}/>
                </TouchableOpacity>

                <Title style={Styles.title}>سگ</Title>

                <TouchableOpacity onPress={() => props.navigation.navigate('Home')} style={{flexDirection: 'row'}}>
                    <View style={Styles.badge}/>
                    <Icon name={'filter'} color={'#4E4E4E'} size={24}/>
                </TouchableOpacity>

            </Appbar.Header>

            <View>
                <InputApp search={true} placeholder="جستجو">

                </InputApp>
            </View>

            <CollectionListItem/>

        </View>
    );
};

export default AdsScreen;
