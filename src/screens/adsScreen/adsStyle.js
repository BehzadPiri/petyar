import {StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const Styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        padding: 10,
    },
    header: {
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    title: {
        fontsize: RFPercentage(2.7),
    },
    badge: {
        width: 5,
        height: 5,
        backgroundColor: '#6AB1D6',
        marginTop: 20,
        borderRadius: 5,
    },

});

export default Styles;
