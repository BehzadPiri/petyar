import {Dimensions, StyleSheet} from 'react-native';

const {width, height} = Dimensions.get('window');

const Styles = StyleSheet.create({

    containerBanner: {
        width: width,
        height: height * 0.15,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imgBanner: {
        position: 'absolute',
        bottom: 15,
    },
    containerImgBG: {
        width: width,
        height: height * 0.55,
        backgroundColor: 'white',
    },
    imgBG: {
        width: null,
        height: null,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    containerViewImgBG: {
        width: width * 0.8,
        height: width * 0.8,
        backgroundColor: 'transparent',
        position: 'absolute',
        bottom: 0,
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
    },

    containerRegisterView: {
        width: width,
        height: height * 0.3,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingBottom: 30,
        paddingTop: 20,
    },

});

export default Styles;
