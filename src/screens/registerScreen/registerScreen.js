import React, {Component} from 'react';
import {Image, ImageBackground, ScrollView, Text, View} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Styles from './registerStyle';
import {BtnApp, InputApp} from '../../components';

class RegisterScreen extends Component {


    render() {

        const onPressBtn = () => {
            this.props.navigation.navigate('Authentication');
        };

        return (
            <ScrollView style={{flex: 1, backgroundColor: 'white'}}>

                <View style={Styles.containerBanner}>
                    <Image style={Styles.imgBanner} source={require('../../assets/petyar.png')}/>
                </View>

                <View style={Styles.containerImgBG}>

                    <ImageBackground style={Styles.imgBG} source={require('../../assets/bgRegister.png')}>

                        <View style={Styles.containerViewImgBG}>

                            <Image source={require('../../assets/bgLabel.png')}/>

                        </View>

                    </ImageBackground>

                </View>


                <View style={Styles.containerRegisterView}>

                    <Text style={{fontSize: RFPercentage(2.5)}}>شماره تماس</Text>

                    <InputApp max={11} keyboardType={'phone-pad'} placeholder={'09121234567'} phone={true}/>

                    <BtnApp text={'تایید شماره موبایل'} onPressBtn={onPressBtn} center={true}/>

                </View>

            </ScrollView>
        );
    }
}

export default RegisterScreen;
