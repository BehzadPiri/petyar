import {StyleSheet} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingTop: 30,
    },
    containerBg: {
        width: '100%',
        height: '50%',
    },
    bgImg: {
        width: null,
        height: null,
        flex: 1,
    },
    header: {
        paddingLeft: 15,
        backgroundColor: 'transparent',
        bottom: 15,
    },
    containerText: {
        width: '100%',
        height: '28%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
    },
    title: {
        fontsize: RFPercentage(2.7),
    },
    scrollText: {
        marginTop: 10,
    },
    text: {
        textAlign: 'justify',
        fontSize: RFPercentage(2.7),
    },
    containerBottom: {
        width: '100%',
        alignItems: 'center',
    },
    containerPrice:{
        width: '80%',
        height: 40,
        borderRadius: 30,
        backgroundColor: '#F0F7FB',
        marginBottom: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10,
    },
    price:{
        color: '#6AB1D6',
        fontSize: RFPercentage(2.8)
    }
});

export default Styles;
