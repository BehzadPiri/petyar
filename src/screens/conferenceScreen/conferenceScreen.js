import React from 'react';
import {ImageBackground, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Appbar, Title} from 'react-native-paper';
import {BtnApp} from '../../components';
import Styles from './conferenceStyle';

const ConferenceScreen = (props) => {

    return (
        <View style={Styles.container}>

            <View style={Styles.containerBg}>

                <ImageBackground style={Styles.bgImg} source={require('../../assets/conference.png')}>

                    <Appbar.Header style={Styles.header}>

                        <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>

                            <Icon name={'arrow-right'} color={'#4E4E4E'} size={24}/>

                        </TouchableOpacity>

                    </Appbar.Header>

                </ImageBackground>

            </View>

            <View style={Styles.containerText}>

                <Title style={Styles.title}>درخواست مشاوره برای خرید حیوانات خانگی</Title>

                <ScrollView style={Styles.scrollText}>

                    <Text style={Styles.text}>
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است،
                        لورم ایپسوم متن ساختگی
                    </Text>

                </ScrollView>

            </View>

            <View style={Styles.containerBottom}>

                <View style={Styles.containerPrice}>

                    <Text style={Styles.price}>مبلغ مشاوره</Text>

                    <Text style={Styles.price}>50,000 تومان</Text>

                </View>

                <BtnApp center={true} text="پرداخت و شروع مشاوره"/>

            </View>

        </View>
    );
};


export default ConferenceScreen;
