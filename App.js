import React, {Component} from 'react';
import {I18nManager} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import StackNavigation from './src/AppNavigation/StackNavigation';

I18nManager.forceRTL(true);


class App extends Component {

    componentDidMount() {
        SplashScreen.hide();
    }

    render() {

        return (

            <StackNavigation/>
        );
    }
}

export default App;
